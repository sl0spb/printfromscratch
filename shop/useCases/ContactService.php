<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 06.12.2018
 * Time: 22:00
 */

namespace shop\useCases;


use shop\forms\ContactForm;
use yii\mail\MailerInterface;

class ContactService
{

	private $adminEmail;

	private $mailer;

	/**
	 * ContactService constructor.
	 *
	 * @param                           $adminEmail
	 * @param \yii\mail\MailerInterface $mailer
	 */
	public function __construct($adminEmail, MailerInterface $mailer)
	{
		$this->adminEmail = $adminEmail;
		$this->mailer = $mailer;
	}

	public function send(ContactForm $form): void
	{
		$sent = $this->mailer->compose()
			->setTo($this->adminEmail)
			->setSubject($form->subject)
			->setTextBody($form->body)
			->send();

		if (!$sent) {
			throw new \RuntimeException('Sending error');
		}
	}
}