<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 08.12.2018
 * Time: 13:41
 */

namespace shop\useCases\auth;


use shop\entities\User\User;
use shop\forms\auth\LoginForm;
use shop\repositories\UserRepository;

class AuthService
{

	private $users;

	/**
	 * AuthService constructor.
	 *
	 * @param $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	public function auth(LoginForm $form): User
	{
		$user = $this->users->findByUsernameOrEmail($form->username);
		if (!$user || !$user->isActive() || !$user->validatePassword($form->password)) {
			throw new \DomainException('Undefined user of password');
		}

		return $user;
}
}