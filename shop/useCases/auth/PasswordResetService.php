<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 05.12.2018
 * Time: 23:01
 */

namespace shop\useCases\auth;

use shop\repositories\UserRepository;
use shop\forms\auth\PasswordResetRequestForm;
use shop\forms\auth\ResetPasswordForm;
use Yii;
use yii\mail\MailerInterface;

class PasswordResetService
{

	private $mailer;
	private $users;

	/**
	 * PasswordResetService constructor.
	 *
	 * @param yii\mail\MailerInterface          $mailer
	 * @param \shop\repositories\UserRepository $users
	 */
	public function __construct(MailerInterface $mailer, UserRepository $users)
	{
		$this->mailer = $mailer;
		$this->users = $users;
	}

	public function request(PasswordResetRequestForm $form): void
	{

		$user = $this->users->getByEmail($form->email);

		if (!$user->isActive()) {
			throw new \DomainException('User is not active');
		}

		$user->requestPasswordReset();
		$this->users->save($user);

		$sent = $this->mailer
			->compose(
				['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
				['user' => $user]
			)
			//->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' robot'])
			->setTo($user->email)
			->setSubject('Password reset for '.Yii::$app->name)
			->send();

		if (!$sent) {
			throw new \RuntimeException('Sending error');
		}
	}

	public function reset(string $token, ResetPasswordForm $form): void
	{
		$user = $this->users->getByPasswordResetToken($token);
		$user->resetPassword($form->password);
		$this->users->save($user);
	}

	public function validateToken($token): void
	{
		if (empty($token) || !is_string($token)) {
			throw new \DomainException('Password reset token cannot be blank.');
		}

		if (!$this->users->existByPasswordResetToken($token)) {
			throw new \DomainException('Wrong password reset token.');
		}
	}

}