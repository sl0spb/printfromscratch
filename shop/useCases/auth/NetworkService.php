<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 09.12.2018
 * Time: 13:52
 */

namespace shop\useCases\auth;


use shop\entities\User\User;
use shop\repositories\UserRepository;

class NetworkService
{

	private $users;

	/**
	 * NetworkService constructor.
	 *
	 * @param $users
	 */
	public function __construct(UserRepository $users)
	{
		$this->users = $users;
	}

	public function auth($network, $identity): User
	{
		if ($user = $this->users->findByNetworkIdentity($network, $identity)) {
			return $user;
		}

		$user = User::signupByNetwork($network, $identity);
		$this->users->save($user);

		return $user;
	}

    public function attach($id, $network, $identity): void
    {
        if ($this->users->findByNetworkIdentity($network, $identity)) {
            throw new \DomainException('Network is already signed up.');
        }

        $user = $this->users->get($id);
        $user->attachNetwork($network, $identity);
        $this->users->save($user);

	}
}