<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 03.12.2018
 * Time: 23:05
 */

namespace common\tests\unit\entities\User;

use common\entities\User;
use Codeception\Test\Unit;

class ConfirmSignupTest extends Unit
{

	public function testSucess()
	{
		$user = new User(
			[
				'status'              => User::STATUS_WAIT,
				'email_confirm_token' => 'token',
			]
		);
		$user->confirmSignup();

		$this->assertEmpty($user->email_confirm_token);
		$this->assertFalse($user->isWait());
		$this->assertTrue($user->isActive());

	}

	public function testAlreadyRegister()
	{
		$user = new User(
			[
				'status'              => User::STATUS_ACTIVE,
				'email_confirm_token' => null,
			]
		);

		$this->expectExceptionMessage('User is already active');

		$user->confirmSignup();
	}
}
