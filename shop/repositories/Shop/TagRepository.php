<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace shop\repositories\Shop;


use shop\entities\Shop\Tag;
use shop\repositories\NotFoundException;

class TagRepository
{
    public function get($id): Tag
    {
        $tag = Tag::findOne($id);
        if (!$tag) {
            throw new NotFoundException('Tag is not found');
        }

        return $tag;
    }

    public function remove(Tag $tag): void
    {
        if (!$tag->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function save(Tag $tag): void
    {
        if (!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function findByName($name): ?Tag
    {
        return Tag::findOne(['name' => $name]);
    }


}