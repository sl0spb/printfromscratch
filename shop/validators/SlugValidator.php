<?php

namespace shop\validators;

use yii\validators\RegularExpressionValidator;

class SlugValidator extends RegularExpressionValidator
{

    public $pattern =
        /** @lang RegExp */
        '%^[a-z0-9_-]*$%s';
    public $message = 'Only [a-z0-9_-] symbols are allowed.';
}