<?php

namespace shop\services\sitemap;

class MapItem
{
    public const  ALWAYS = 'always';
    public const  HOURLY = 'hourly';
    public const   DAILY = 'daily';
    public const  WEEKLY = 'weekly';
    public const MONTHLY = 'monthly';
    public const  YEARLY = 'yearly';
    public const   NEVER = 'never';

    public $location;
    public $lastModified;
    public $changeFrequency;
    public $priority;

    public function __construct($location, $lastModified = null, $changeFrequency = null, $priority = null)
    {
        $this->location = $location;
        $this->lastModified = $lastModified;
        $this->changeFrequency = $changeFrequency;
        $this->priority = $priority;
    }
}