<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 10.12.2018
 * Time: 23:28
 */

namespace shop\helpers;


use shop\entities\User\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class UserHelper
{

    public static function statusLabel($status): string
    {
        switch ($status) {
            case User::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case User::STATUS_WAIT:
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    public static function statusList(): array
    {
        return [
            User::STATUS_WAIT   => 'Wait',
            User::STATUS_ACTIVE => 'Active',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }
}