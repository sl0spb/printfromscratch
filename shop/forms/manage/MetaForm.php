<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 15.12.2018
 * Time: 2:13
 */

namespace shop\forms\manage;


use shop\entities\Meta;
use yii\base\Model;

class MetaForm extends Model
{
    public $description;
    public $keywords;
    public $title;

    public function __construct(Meta $meta = null, $config = [])
    {
        if ($meta) {
            $this->title = $meta->title;
            $this->description = $meta->description;
            $this->keywords = $meta->keywords;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['description', 'keywords'], 'string'],
        ];
    }

}