<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 15.12.2018
 * Time: 1:46
 */

namespace shop\forms\manage\Shop;


use shop\entities\Shop\Brand;
use shop\forms\CompositeForm;
use shop\forms\manage\MetaForm;
use shop\validators\SlugValidator;


/**
 * @property \shop\forms\manage\MetaForm meta
 */
class BrandForm extends CompositeForm
{
    public $name;
    public $slug;

    private $_brand;


    /**
     * TagForm constructor.
     *
     * @param \shop\entities\Shop\Brand $brand
     * @param array                     $config
     */
    public function __construct(Brand $brand = null, $config = [])
    {
        if ($brand) {
            $this->name = $brand->name;
            $this->slug = $brand->slug;
            $this->_brand = $brand;
            $this->meta = new MetaForm($brand->meta);
        } else {
            $this->meta = new MetaForm();
        }
        parent::__construct($config);
    }

    protected function internalForms(): array
    {
        return ['meta'];
    }

    public function load($data, $formName = null): bool
    {
        $self = parent::load($data, $formName);
        $meta = $this->meta->load($data);

        return $self && $meta;
    }

    public function rules(): array
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => Brand::class, 'filter' => $this->_brand ? ['<>', 'id', $this->_brand->id] : null],
        ];
    }
}