<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 09.12.2018
 * Time: 11:41
 */

namespace shop\entities\User;


use Webmozart\Assert\Assert;
use yii\db\ActiveRecord;

/**
 * @property  string $network
 * @property  string $identity
 * @property int     $id
 * @property int     $user_id
 */
class Network extends ActiveRecord
{

    public static function create($network, $identity): self
    {
        Assert::notEmpty($network);
        Assert::notEmpty($identity);

        $item = new static();
        $item->network = $network;
        $item->identity = $identity;

        return $item;
    }

    public function isFor($network, $identity): bool
    {
        return ($this->network === $network && $this->identity === $identity);
    }

    public static function tableName(): string
    {
        return '{{%user_networks}}';
    }
}