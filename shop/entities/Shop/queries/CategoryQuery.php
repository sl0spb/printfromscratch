<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 15.12.2018
 * Time: 15:38
 */

namespace shop\entities\Shop\queries;

use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}