<?php

use shop\entities\User\User;

return [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => User::class,
        ],
        'request' => [
            'cookieValidationKey' => 'test',
        ],
    ],
];
