<?php
/** @noinspection SummerTimeUnsafeTimeManipulationInspection */
return [
    'adminEmail'                    => 'admin@example.com',
    'supportEmail'                  => 'support@example.com',
    'user.rememberMeDuration'       => 3600 * 24 * 30,
    'user.passwordResetTokenExpire' => 3600,
    'cookieDomain'                  => '.example.com',
    'frontendHostInfo'              => 'http://example.com',
    'backendHostInfo'               => 'http://backend.example.com',
    'staticHostInfo'                => 'http://static.scratch.test',
    'staticPath'                    => dirname(__DIR__, 2) . '/static',
    'mailChimpKey'                  => '',
    'mailChimpListId'               => '',
];
