<?php

//use yii\caching\MemCache;

use yii\rbac\DbManager;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'bootstrap'  => [
        'queue',
    ],
//	'bootstrap'  => ['common\bootstrap\SetUp'],

    'components' => [
        /*        'cache' => [
                    'class'        => MemCache::class,
                    'servers'      => [
                        [
                            'host' => 'localhost',
                            'port' => 11211,
                            'weight' => 100,
                        ],
                    ],
                    'useMemcached' => true,
                ],*/
        'cache' => [
            'class'     => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],

        'authManager' => [
            'class'           => DbManager::class,
            'itemTable'       => '{{%auth_items}}',
            'itemChildTable'  => '{{%auth_item_children}}',
            'assignmentTable' => '{{%auth_assignments}}',
            'ruleTable'       => '{{%auth_rules}}',
        ],
        'queue'       => [
            'class'  => 'yii\queue\redis\Queue',
            'as log' => 'yii\queue\LogBehavior',
        ],

    ],
];

