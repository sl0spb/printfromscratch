<?php

use yii\db\Migration;

/**
 * Class m181206_223827_add_user_email_confirm_token
 */
class m181206_223827_add_user_email_confirm_token extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
	    $this->addColumn('{{%user}}', 'email_confirm_token', $this->string()->unique()->after('email'));
    }

    public function down()
    {
	    $this->dropColumn('{{%user}}', 'email_confirm_token');
    }

}
