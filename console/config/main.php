<?php

use common\bootstrap\SetUp;
use yii\log\FileTarget;
use fishvision\migrate\controllers\MigrateController;
use yii\console\controllers\FixtureController;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        SetUp::class,
    ],
    'controllerNamespace' => 'console\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap'       => [
        'fixture' => [
            'class'     => FixtureController::class,
            'namespace' => 'common\fixtures',
        ],
        'migrate' => [
            'class'          => MigrateController::class,
            'autoDiscover'   => true,
            'migrationPaths' => [
                '@vendor/yiisoft/yii2/rbac/migrations',
            ],
        ],
    ],

    'components' => [
        'log'                => [
            'targets' => [
                [
                    'class'  => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'backendUrlManager'  => require __DIR__ . '/../../backend/config/urlManager.php',
    ],
    'params'     => $params,
];
