<?php

use shop\helpers\UserHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model shop\entities\User\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
	</p>

	<div class="box">
		<div class="box-body">
            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    'id',
                    'username',
                    'email:email',
                    [
                        'attribute' => 'status',
                        'value'     => UserHelper::statusLabel($model->status),
                        'format'    => 'raw',
                    ],
                    [
                        'label'  => 'Role',
                        'value'  => implode(', ', ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($model->id), 'description')),
                        'format' => 'raw',
                    ],

                    [
                        'attribute' => 'created_at',
                        'format'    => ['date', 'HH:mm:ss dd.MM.YYYY'],
                        'options'   => ['width' => '200'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format'    => ['date', 'HH:mm:ss dd.MM.YYYY'],
                        'options'   => ['width' => '200'],
                    ],
                ],
            ]) ?>
		</div>
	</div>

</div>
