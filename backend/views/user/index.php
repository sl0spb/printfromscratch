<?php

use backend\widgets\grid\RoleColumn;
use kartik\date\DatePicker;
use shop\entities\User\User;
use shop\helpers\UserHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<div class="box">
		<div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    //                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'username',
                    'email:email',
                    [
                        'attribute' => 'role',
                        'class' => RoleColumn::class,
                        'filter' => $searchModel->rolesList(),
                    ],

                    [
                        'attribute'          => 'status',
                        'filter'             => UserHelper::statusList(),
                        'filterInputOptions' => ['class' => 'form-control'],
                        'value'              => function (User $user) {
                            return UserHelper::statusLabel($user->status);
                        },
                        'format'             => 'raw',
                        'options'            => ['width' => '110'],
                    ],
                    //                    'created_at:datetime',
                    [
                        'attribute' => 'created_at',
                        'filter'    => DatePicker::widget([
                            'model'         => $searchModel,
                            'attribute'     => 'date_from',
                            'attribute2'    => 'date_to',
                            'type'          => DatePicker::TYPE_RANGE,
                            'separator'     => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose'      => true,
                                'format'         => 'yyyy-mm-dd',
                            ],
                        ]),
                        'format'    => ['date', 'HH:mm:ss dd.MM.YYYY'],
                    ],


                    /*                    [
                                            'attribute' => 'created_at',
                                            'format'    => ['date', 'HH:mm:ss dd.MM.YYYY'],
                                            'options'   => ['width' => '200'],
                                        ],*/

                    ['class' => ActionColumn::class],
                ],
            ]); ?>
		</div>
	</div>
</div>
