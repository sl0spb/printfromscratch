<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class OwlCarouselAsset extends AssetBundle
{
    public $css = [
        'assets/owl.carousel.css',
    ];
    public $cssOptions = [
        'media' => 'screen',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    public $js = [
        'owl.carousel.js',
    ];
    public $sourcePath = '@bower/owl-carousel2/dist';
}
