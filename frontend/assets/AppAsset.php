<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Lora%3A400%2C400italic%7CMontserrat%3A400%2C700&#038;subset=latin%2Clatin-ext',
        'css/style.css',
    ];
    public $js = [
        'js/common.js',
    ];
    public $depends = [
        'frontend\assets\FontAwesomeAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset', // BootstrapAsset - подключает только css, BootstrapPluginAsset еще и js
    ];
}
