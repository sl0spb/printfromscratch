<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\web\ErrorAction;
use yii\captcha\CaptchaAction;

/**
 * Site controller
 */
class SiteController extends Controller
{

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error'   => [
				'class' => ErrorAction::class,
			],
			'captcha' => [
				'class'           => CaptchaAction::class,
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}


	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
        $this->layout = 'home';
		return $this->render('index');
	}


}
