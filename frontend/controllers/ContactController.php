<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 08.12.2018
 * Time: 20:24
 */

namespace frontend\controllers;


use shop\forms\ContactForm;
use shop\useCases\ContactService;
use Yii;
use yii\web\Controller;

class ContactController extends Controller
{

	private $contactService;

	/**
	 * SiteController constructor.
	 *
	 * @param                                              $id
	 * @param                                              $module
	 * @param \shop\useCases\ContactService                $contactService
	 * @param array                                        $config
	 */
	public function __construct($id, $module, ContactService $contactService, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->contactService = $contactService;
	}

	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$form = new ContactForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$this->contactService->send($form);
				Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');

				return $this->goHome();
			} catch (\Exception $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', 'There was an error sending your message');
			}

			return $this->refresh();
		}

		return $this->render(
			'index',
			[
				'model' => $form,
			]
		);
	}
}