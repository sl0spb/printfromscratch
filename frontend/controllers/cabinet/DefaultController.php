<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 10.12.2018
 * Time: 1:11
 */

namespace frontend\controllers\cabinet;

use yii\filters\AccessControl;
use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout = 'cabinet';

    public function actionIndex(): string
    {
        return $this->render('index');
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

}