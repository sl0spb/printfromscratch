<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 08.12.2018
 * Time: 20:36
 */

namespace frontend\controllers\auth;

use shop\forms\PasswordResetRequestForm;
use shop\forms\ResetPasswordForm;
use shop\useCases\auth\PasswordResetService;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ResetController extends Controller
{
    public $layout = 'cabinet';
	private $service;

	/**
	 * SiteController constructor.
	 *
	 * @param                                              $id
	 * @param                                              $module
	 * @param \shop\useCases\auth\PasswordResetService     $passwordResetService
	 * @param array                                        $config
	 */
	public function __construct($id, $module, \shop\useCases\auth\PasswordResetService $passwordResetService, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->service = $passwordResetService;
	}

	/**
	 * Resets password.
	 *
	 * @param string $token
	 *
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionConfirm($token)
	{
		$service = $this->service;
		try {
			$service->validateToken($token);
		} catch (\DomainException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		$form = new ResetPasswordForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$service->reset($token, $form);
				Yii::$app->session->setFlash('success', 'New password saved.');

				return $this->goHome();
			} catch (\DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}

			return $this->goHome();
		}

		return $this->render(
			'confirm',
			['model' => $form,]
		);
	}

	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequest()
	{
		$form = new PasswordResetRequestForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$this->service->request($form);
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} catch (\DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render(
			'request',
			[
				'model' => $form,
			]
		);
	}
}