<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 09.12.2018
 * Time: 14:42
 */

namespace frontend\controllers\auth;

use common\auth\Identity;
use shop\useCases\auth\NetworkService;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class NetworkController extends Controller
{

    private $service;

    /**
     * NetworkController constructor.
     *
     * @param                                    $id
     * @param                                    $module
     * @param \shop\useCases\auth\NetworkService $service
     * @param array                              $config
     */
    public function __construct($id, $module, NetworkService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;

    }

    public function actions()
    {
        return [
            'auth' => [
                'class'           => AuthAction::class,
                'successCallback' => [
                    $this,
                    'onAuthSuccess',
                ],
            ],
        ];
    }

    public function onAuthSuccess(ClientInterface $client): void
    {
        $network = $client->getId();
        $attributes = $client->getUserAttributes();
        $identity = ArrayHelper::getValue($attributes, 'id');

        try {
            $user = $this->service->auth($network, $identity);
            Yii::$app->user->login(
                new Identity($user) ,
                Yii::$app->params['user.rememberMeDuration']
            );
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

}