<?php
/**
 * Created by PhpStorm.
 * User: sl0
 * Date: 08.12.2018
 * Time: 20:42
 */

namespace frontend\controllers\auth;


use shop\forms\auth\SignupForm;
use shop\useCases\auth\SignupService;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class SignupController extends Controller
{
    public $layout = 'cabinet';
	private $signupService;


	/**
	 * SiteController constructor.
	 *
	 * @param                                              $id
	 * @param                                              $module
	 * @param \shop\useCases\auth\SignupService            $signupService
	 * @param array                                        $config
	 */
	public function __construct($id, $module, \shop\useCases\auth\SignupService $signupService, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->signupService = $signupService;
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only'  => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow'   => true,
						'roles'   => ['?'],
					],
				],
			],
		];
	}

	public function actionConfirm($token)
	{
		try {
			$this->signupService->confirm($token);
			Yii::$app->session->setFlash('success', 'Your email is confirmed!');

			return $this->redirect('login');
		} catch (\DomainException $e) {
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());

			return $this->goHome();
		}
	}

	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionRequest()
	{
		$form = new SignupForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$this->signupService->signup($form);
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} catch (\DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render(
			'request',
			[
				'model' => $form,
			]
		);
	}


}