<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use frontend\assets\OwlCarouselAsset;
use frontend\widgets\Shop\CartWidget;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
OwlCarouselAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<!--[if IE]><![endif]-->
	<!--[if IE 8 ]>
	<html dir="ltr" lang="en" class="ie8"><![endif]-->
	<!--[if IE 9 ]>
	<html dir="ltr" lang="en" class="ie9"><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!-->
	<html dir="ltr" lang="en">
	<!--<![endif]-->
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<link href="<?= Html::encode(Url::canonical()) ?>" rel="canonical"/>
		<link href="<?= Yii::getAlias('@web/images/cart.png') ?>" rel="icon"/>
        <?php $this->head() ?>
	</head>
	<body class="archive post-type-archive post-type-archive-product woocommerce woocommerce-page woocommerce-no-js">
    <?php $this->beginBody() ?>
	<div id="wrapper">
		<div class="topbar">
            <?php
            NavBar::begin([
                /*                'brandLabel' => Yii::$app->name,
                                'brandUrl' => Yii::$app->homeUrl,
                                'options' => [
                                    'class' => 'my-top-menu',
                                ],*/
            ]);
            $menuItems = [ // left part topbar
                [
                    'label' => 'Home',
                    'url'   => ['/site/index'],
                ],
                ['label' => 'Catalog', 'url' => ['/shop/catalog/index']],
                ['label' => 'Blog', 'url' => ['/blog/post/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/contact']],
            ];

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items'   => $menuItems,
            ]);

            $menuItems = []; // right part topbar
            if (Yii::$app->user->isGuest) {
                $menuItems[] =
                    [
                        'label' => 'Signup',
                        'url'   => ['/signup'],
                    ];
                $menuItems[] = [
                    'label' => 'Login',
                    'url'   => ['/login'],
                ];
            } else {
                $menuItems[] = ['label' => 'My cabinet', 'url' => ['/cabinet/default/index']];
                $menuItems[] = ['label' => 'My Wishlist', 'url' => ['/cabinet/wishlist/index']];
                $menuItems[] = '<li>'
                    . Html::beginForm(['/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->getUsername() . ')',
                        ['class' => 'btn-link']
                    )
                    . Html::endForm()
                    . '</li>';
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items'   => $menuItems,
            ]);
            NavBar::end();
            ?>
		</div>
		<div id="site-name" class="container">
<!--			<h2><a href="http://natalie.az-theme.net/"><img src="http://natalie.az-theme.net/wp-content/themes/natalie/assets/images/logo.png" alt="Natalie"/></a></h2>-->
			<div class="col-sm-5">
                <?= Html::beginForm(['/shop/catalog/search'], 'get') ?>
				<div id="search" class="input-group">
					<input type="text" name="text" value="" placeholder="Search" class="form-control input-lg"/>
					<span class="input-group-btn">
                        <button type="submit" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                    </span>
				</div>
                <?= Html::endForm() ?>
			</div>
			<div class="col-sm-3">
                <?= CartWidget::widget() ?>
			</div>
		</div>
		<div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
		</div>
		<div id="nav-wrapper">
			<div class="container">


                <?= $content ?>

			</div>
		</div>
        <?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>